<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\modules\articles\models\Articles;
use app\models\ContactForm;
use yii\web\HttpException;
use howard\behaviors\iwb\InlineWidgetsBehavior;
use common\helpers\SessionHelper;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public $defaultAction = 'index';

    public function beforeAction($event)
    {
        SessionHelper::popupModal();

        return parent::beforeAction($event);
    }

    public function behaviors()
    {
        return [
            'InlineWidgetsBehavior' => [
                'class' => InlineWidgetsBehavior::className(),
                'widgets' => \Yii::$app->params['runtimeWidgets'],
                'startBlock' => '[*',
                'endBlock' => '*]',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionOrdersend()
    {
        $model = new ContactForm();
        $model->orderform();
        if ($model->contact(Yii::$app->params['contactFormEmails'],"Make order form - New Jodi")) {
            echo "ok";
        } else echo json_encode($model->getErrors());
    }

    public function actionIndex()
    {
        $articles = Articles::find()->where("alias=:alias", [":alias" => "index"])->one();

        if ($articles === null) {
            throw new HttpException(404, "Page not found");
        }

        return $this->render('@app/modules/articles/views/articles/main', ["articles" => $articles]);
    }

    public function actionContact()
    {

        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['contactFormEmails'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');
            return $this->refresh();
        }

        return $this->render('contact', [
            'model' => $model
        ]);
    }


}
