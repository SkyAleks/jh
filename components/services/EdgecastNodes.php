<?php

namespace app\components\services;
use yii\base\Object;

class EdgecastNodes extends Object
{
    private $statuses;

    public function init()
    {
        $this->statuses = [
            '1'=>'https://cdn.jodihost.com/images/ok_24.ico',
            '2'=>'https://cdn.jodihost.com/images/maintenance_24.ico',
            '3'=>'https://cdn.jodihost.com/images/warning_24.ico',
            '4'=>'https://cdn.jodihost.com/images/not_ok_24.ico',
            '5'=>'https://cdn.jodihost.com/images/not_provided.ico'
        ];
        parent::init();

    }

    /**
     * @return mixed
     */
    public function getStatuses()
    {
        return $this->statuses;
    }

}