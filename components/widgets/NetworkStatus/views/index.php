<link rel="stylesheet" media="all" type="text/css" href="https://www.jodihost.com/css/status.css">
<section id="legend">
    <ul>
        <li><img alt="OK" src="https://cdn.jodihost.com/images/ok_24.ico">OK</li>
        <li><img alt="Scheduled Maintenance" src="https://cdn.jodihost.com/images/maintenance_24.ico">Scheduled Maintenance</li>
        <li><img alt="Degraded Performance" src="https://cdn.jodihost.com/images/warning_24.ico">Degraded Performance</li>
        <li><img alt="Not OK" src="https://cdn.jodihost.com/images/not_ok_24.ico">Not OK</li>
        <li><img alt="Service Not Provided" src="https://cdn.jodihost.com/images/not_provided.ico">Service Not Provided</li>
    </ul>
</section>

<div id="status-data">
    <table id="statuses" class="table table-striped">

        <thead>
        <tr>

                    <td align=center>POP Location</td>
                    <td align=center>ADN</td>
                    <td align=center>HTTP-L</td>
                    <td align=center>FMS</td>
                    <td align=center>HTTP-S</td>
                    <td align=center>Purges</td>

        </tr>
        </thead>
        <tbody>
        <?php foreach($locations as $v){ ?>
        <tr>

                    <td><?=$v->location_name?></td>

                    <td align="center"><img class="status-icon" src="<?=$status[$v["ADN"]]?>"></td>
                    <td align="center"><img class="status-icon" src="<?=$status[$v["HTTP-L"]]?>"></td>
                    <td align="center"><img class="status-icon" src="<?=$status[$v["FMS"]]?>"></td>
                    <td align="center"><img class="status-icon" src="<?=$status[$v["HTTP-S"]]?>"></td>
                    <td align="center"><img class="status-icon" src="<?=$status[$v["Purges"]]?>"></td>

        </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
</div>