<?php

namespace app\components\widgets\NetworkStatus;


use yii;
use yii\base\Widget;
use app\models\EdgecastLocations;

class NetworkStatus extends Widget
{


    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $locations = EdgecastLocations::find()->orderby("location_id")->all();
        return $this->render("index", ["locations" => $locations,'status'=>Yii::$app->egnodes->statuses]);
    }
}

?>