<?php

namespace app\components\widgets\TextBlock;

use yii\base\Widget;
use yii\helpers\Html;

class TextBlock extends Widget
{
    public $tpl = null;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        if ($this->tpl != null) return $this->render($this->tpl);
    }
}

?>