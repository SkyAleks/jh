<?php

namespace app\models;
use Yii;

class EdgecastStatuses extends \yii\db\ActiveRecord
{

    public static function getDb()
    {
        return \Yii::$app->db;
    }


    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */

    public static function tableName()
    {
        return 'edgecast_statuses';
    }

}
