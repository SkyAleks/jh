<?php

namespace app\models;
use Yii;

class EdgecastLocations extends \yii\db\ActiveRecord
{

    public static function getDb()
    {
        return \Yii::$app->db;
    }


    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */

    public static function tableName()
    {
        return 'edgecast_locations';
    }

}
