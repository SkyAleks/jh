<?php

namespace app\modules\articles\models;
use Yii;

class Articles extends \yii\db\ActiveRecord
{

    public static function getDb()
    {
        return \Yii::$app->db;
    }


    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @inheritdoc
     */

    public static function tableName()
    {
        return 'articles';
    }

}
