<?php
/**
 * Created by PhpStorm.
 * User: Sky
 * Date: 05.12.2016
 * Time: 13:23
 */

namespace app\modules\articles\controllers;

use yii;
use yii\web\Controller;
use app\modules\articles\models\Articles;
use howard\behaviors\iwb\InlineWidgetsBehavior;
use yii\web\HttpException;
use common\helpers\SessionHelper;

class ArticlesController extends Controller
{
    public $menus;

    public function beforeAction($event)
    {
        SessionHelper::popupModal();

        return parent::beforeAction($event);
    }

    public function behaviors()
    {
        return [
            'InlineWidgetsBehavior' => [
                'class' => InlineWidgetsBehavior::className(),
                //'namespace'=> 'components\widgets\TextBlock', // default namespace (optional)
                'widgets' => \Yii::$app->params['runtimeWidgets'],
                'startBlock' => '[*',
                'endBlock' => '*]',
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays selected page.
     *
     * @return string
     */
    public function actionView()
    {
        $alias = Yii::$app->request->get('alias');
        $articles = Articles::find()->where("alias=:alias", [":alias" => $alias])->one();
        if ($articles === null) {

            throw new HttpException(404, "Paage not found");
        }

        return $this->render($articles->template, ["articles" => $articles]);
    }

}