<?php

use common\helpers\BreadcrumbsHelper;

$this->title =  $articles->metaTitle;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $articles->metaDescription,
]);

Yii::$app->view->registerMetaTag([
    'name' => 'keywords',
    'content' => $articles->metaKeywords,
]);
echo BreadcrumbsHelper::articles($articles);
Yii::$app->params['activeMenu']=$articles->activeMenu;
?>

<?php if ($articles->textBeforeImage != ""): ?>
<!--Box-->
<div class="box title"><?= $articles->textBeforeImage ?></div>
<!--Box-->
<?php endif; ?>
<?php if ($articles->image != ""): ?>
<div class="box illustration">
    <?php if ($articles->imageText != ""): ?>
    <div class="slogans">
        <?=$articles->imageText?>
    </div>
    <?php endif; ?>
    <div class="bgi"><img src="<?=$articles->image?>" alt="illustration"></div>
</div>
<!--//Box-->
<?php endif; ?>
<?php if ($articles->shortText != ""): ?>
<div class="box">
    <?=$articles->shortText?>
</div>
<?php endif; ?>

<?php echo $this->context->decodeWidgets($articles->widgetsBeforeText); ?>

<?php if ($articles->title != ""): ?>
    <!--Title Box-->
    <h1 class="box title"><?= $articles->title ?></h1>
    <!--End Title Box-->
<?php endif; ?>
<?php if ($articles->body != ""): ?>
    <!--//Body Box-->
    <div class="box">
        <?php echo $this->context->decodeWidgets($articles->body); ?>
    </div>
    <!--//End Body Box-->
<?php endif; ?>
<?php echo $this->context->decodeWidgets($articles->widgetsAfterText); ?>
