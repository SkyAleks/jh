<?php

//use app\modules\articles\assets\MainAsset;
//$theme=MainAsset::register($this);
//echo $theme->baseUrl;
use common\helpers\BreadcrumbsHelper;

$this->title =  $articles->metaTitle;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $articles->metaDescription,
]);

Yii::$app->view->registerMetaTag([
    'name' => 'keywords',
    'content' => $articles->metaKeywords,
]);
echo BreadcrumbsHelper::articles($articles);
Yii::$app->params['activeMenu']=$articles->activeMenu;
?>
<?php if ($articles->image != ""): ?>
    <div class="box illustration">
        <?php if ($articles->imageText != ""): ?>
            <div class="slogans">
                <?=$articles->imageText?>
            </div>
        <?php endif; ?>
        <div class="bgi"><img src="<?=$articles->image?>" alt="illustration"></div>
    </div>
    <!--//Box-->
<?php endif; ?>

<?php echo $this->context->decodeWidgets($articles->body); ?>
<?php echo $this->context->decodeWidgets($articles->widgetsAfterText); ?>


