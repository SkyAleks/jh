<?php
/**
 * Created by PhpStorm.
 * User: Sky
 * Date: 05.12.2016
 * Time: 13:19
 */

namespace app\modules\articles;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\articles\controllers';

   // public $layout = '@app/themes/main/layouts/main';

    public function init()
    {
        parent::init();

    }
}